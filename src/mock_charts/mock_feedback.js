import { GridWidget } from "@polaris/dashboard-sdk";

const data = [
    {
        type: "textAreaElement",
        name: "",
        options: {
            showLegend: false,
        },
        data: {
            text: "Gib hier dein Feedback ein",
        }
    }
]


export function mockFeedback(onShowDesc) {
    return {
        "feedback-grid-widget":
            new GridWidget(
                "Feedback",
                "Feedback Element",
                data,
                {
                    direction: "column",
                    onShowDesc: onShowDesc
                })
    }
}

import { GridWidget } from "@polaris/dashboard-sdk";

const data = [
    {
        type: "grid",
        name: "Block 1",
        options: {
            direction: "column",
        },
        data: [
            {
                type: "piechart",
                name: "",
                options: {
                    showLegend: false
                },
                data:
                    [
                        {column1: "bestanden", column2: 75},
                        {column1: "durchgefallen", column2: 25},
                    ]
            },
            {
                type: "piechart",
                name: "",
                options: {
                    showLegend: false
                },
                data:
                    [
                        {column1: "bestanden", column2: 68},
                        {column1: "durchgefallen", column2: 32},
                    ]
            },
            {
                type: "piechart",
                name: "",
                options: {
                    showLegend: false
                },
                data:
                    [
                        {column1: "bestanden", column2: 91},
                        {column1: "durchgefallen", column2: 9},
                    ]
            }
        ]
    },
    {
        type: "grid",
        name: "Block 2",
        options: {
            direction: "column",
        },
        data: [
            {
                type: "piechart",
                name: "",
                options: {
                    showLegend: false
                },
                data:
                    [
                        {column1: "bestanden", column2: 83},
                        {column1: "durchgefallen", column2: 17},
                    ]
            },
            {
                type: "piechart",
                name: "",
                options: {
                    showLegend: false
                },
                data:
                    [
                        {column1: "bestanden", column2: 91},
                        {column1: "durchgefallen", column2: 9},
                    ]
            },
            {
                type: "piechart",
                name: "",
                options: {
                    showLegend: false
                },
                data:
                    [
                        {column1: "bestanden", column2: 76},
                        {column1: "durchgefallen", column2: 24},
                    ]
            }
        ]
    },
    {
        type: "grid",
        name: "Block 2",
        options: {
            direction: "column",
        },
        data: [
            {
                type: "piechart",
                name: "",
                options: {
                    showLegend: false
                },
                data:
                    [
                        {column1: "bestanden", column2: 95},
                        {column1: "durchgefallen", column2: 5},
                    ]
            },
            {
                type: "piechart",
                name: "",
                options: {
                    showLegend: false
                },
                data:
                    [
                        {column1: "bestanden", column2: 35},
                        {column1: "durchgefallen", column2: 65},
                    ]
            },
            {
                type: "piechart",
                name: "",
                options: {
                    showLegend: false
                },
                data:
                    [
                        {column1: "bestanden", column2: 45},
                        {column1: "durchgefallen", column2: 55},
                    ]
            }
        ]
    }
]


export function mockPieGrid(onShowDesc)
{
    return { "mc-pie-grid-widget" :
            new GridWidget(
                "MC-Tests",
                "Vergleich von Multiple-Choice Elementen",
                data,
                {
                    direction: "row",
                    onShowDesc: onShowDesc
                })
    }
}
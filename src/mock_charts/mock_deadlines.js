import { GridWidget } from "@polaris/dashboard-sdk";

const data = [
    {
        type: "textElement",
        name: "Block 1",
        options: {
            showLegend: false,
        },
        data: {
            text: "00.00.00 - noch ... Tage"
        }
    },
    {
        type: "textElement",
        name: "Block 2",
        options: {
            showLegend: false,
        },
        data: {
            text: "00.00.00 - noch ... Tage"
        }
    },
    {
        type: "textElement",
        name: "Block 3",
        options: {
            showLegend: false,
        },
        data: {
            text: "00.00.00 - noch ... Tage"
        }
    },
]


export function mockDeadlines(onShowDesc) {
    return {
        "deadlines-grid-widget":
            new GridWidget(
                "Deadlines",
                "Deadlines verschiedener Blöcke",
                data,
                {
                    direction: "column",
                    onShowDesc: onShowDesc
                })
    }
}
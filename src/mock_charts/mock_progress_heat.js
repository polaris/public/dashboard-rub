import { GridWidget } from "@polaris/dashboard-sdk";

const data = [
    {
        type: "heatmap",
        name: "Block 1",
        options: {
            showLegend: false,
        },
        data:
            [
                {
                    group: "A",
                    variable: "v1",
                    value: 12
                },
                {
                    group: "B",
                    variable: "v1",
                    value: 82
                },
                {
                    group: "C",
                    variable: "v1",
                    value: 23
                },
                {
                    group: "D",
                    variable: "v1",
                    value: 89
                },
                {
                    group: "E",
                    variable: "v1",
                    value: 75
                },
                {
                    group: "F",
                    variable: "v1",
                    value: 38
                },
                {
                    group: "G",
                    variable: "v1",
                    value: 25
                },
            ]
    },
    {
        type: "heatmap",
        name: "Block 2",
        options: {
            showLegend: false,
        },
        data:
            [
                {
                    group: "A",
                    variable: "v1",
                    value: 12
                },
                {
                    group: "B",
                    variable: "v1",
                    value: 82
                },
                {
                    group: "C",
                    variable: "v1",
                    value: 23
                },
                {
                    group: "D",
                    variable: "v1",
                    value: 89
                },
                {
                    group: "E",
                    variable: "v1",
                    value: 75
                },
                {
                    group: "F",
                    variable: "v1",
                    value: 38
                },
                {
                    group: "G",
                    variable: "v1",
                    value: 25
                },
            ]
    },
    {
        type: "heatmap",
        name: "Block 3",
        options: {
            showLegend: false,
        },
        data:
            [
                {
                    group: "A",
                    variable: "v1",
                    value: 12
                },
                {
                    group: "B",
                    variable: "v1",
                    value: 82
                },
                {
                    group: "C",
                    variable: "v1",
                    value: 23
                },
                {
                    group: "D",
                    variable: "v1",
                    value: 89
                },
                {
                    group: "E",
                    variable: "v1",
                    value: 75
                },
                {
                    group: "F",
                    variable: "v1",
                    value: 38
                },
                {
                    group: "G",
                    variable: "v1",
                    value: 25
                },
            ]
    }
]


export function mockProgressHeat(onShowDesc) {
    return { "heat-progress-widget" :
            new GridWidget(
                "Kursfortschritt",
                "Fortschritt in den einzelnen Kursblöcken",
                data,
                {
                    direction: "column",
                    onShowDesc: onShowDesc
                })
    }
}

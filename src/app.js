import "./style.css";

import { initGrid } from "@polaris/dashboard-sdk/dist/bundle";
import {mockBarChart, mockChartJSBarChart} from "./mock_charts/mock_bar_chart";
import {mockGrid} from "./mock_charts/mock_grid";
import {mockScatterGrid} from "./mock_charts/mock_scatter_grid";
import {mockPieGrid} from "./mock_charts/mock_pie_grid";
import {mockDeadlines} from "./mock_charts/mock_deadlines";
import {mockProgressHeat} from "./mock_charts/mock_progress_heat";
import {mockProgressBox} from "./mock_charts/mock_progress_box";
import {mockFeedback} from "./mock_charts/mock_feedback";
import {mockLineChart} from "./mock_charts/mock_line_chart";

/**
 * Setup initial widget position and sizes
 */
const widgets_config = [
    {
        x: 0,
        y: 0,
        w: 4,
        h: 6,
        widgetId: "scatter-grid-widget",
    },
    {
        x: 4,
        y: 0,
        w: 4,
        h: 6,
        widgetId: "grid-widget",
    },
    {
        x: 8,
        y: 0,
        w: 4,
        h: 7,
        widgetId: "chartjs-barchart-widget",
    },

    {
        x: 0,
        y: 6,
        w: 2,
        h: 5,
        widgetId: "heat-progress-widget",
    },
    {
        x: 2,
        y: 6,
        w: 2,
        h: 5,
        widgetId: "box-progress-widget"
    },
    {
        x: 0,
        y: 11,
        h: 6,
        w: 4,
        widgetId: "line-chart-widget"
    },
    {
        x: 4,
        y: 6,
        w: 4,
        h: 8,
        widgetId: "mc-pie-grid-widget",
    },
    {
        x: 8,
        y: 6,
        w: 4,
        h: 3,
        widgetId: "deadlines-grid-widget",
    },
    {
        x: 8,
        y: 9,
        w: 4,
        h: 5,
        widgetId: "feedback-grid-widget",
    }
];

/**
 * Handle description callback for widgets.
 * Shows modal with analytics engine description.
 * @param {*} desc
 */
const onShowDesc = (desc) => {
    const modalContent = document.getElementById("modal-content-body");
    modalContent.innerText = desc;
    const modal = new bootstrap.Modal(document.getElementById("myModal"), {});
    modal.show();
};

/**
 * Opens bootstrap error modal.
 * @param {*} message
 */
const showErrorModal = (message) => {
    const modalContent = document.getElementById("error-modal-content-body");
    modalContent.innerText = message;
    const modal = new bootstrap.Modal(document.getElementById("myErrorModal"), {});
    modal.show();
};

/**
 *  Built widgets from results data.
 * @param {*} data
 * @returns
 */
const buildWidgets = (data) => {
    const widgets = {
        //...mockBarChart,
        ...mockChartJSBarChart(onShowDesc),
        ...mockGrid(onShowDesc),
        ...mockScatterGrid(onShowDesc),
        ...mockPieGrid(onShowDesc),
        ...mockDeadlines(onShowDesc),
        ...mockProgressHeat(onShowDesc),
        ...mockProgressBox(onShowDesc),
        ...mockFeedback(onShowDesc),
        ...mockLineChart(onShowDesc)
    };

    return widgets;
};

const setupGrid = (data) => {
    //  Create widgets with data from analytics engine results

    const widgets = buildWidgets(data);

    // Initialize grid with widgets at configured positions
    grid = initGrid(widgets, widgets_config);

    // Handle toggle button click
    const toggleBtn = document.getElementById("toggle-sidebar-btn");
    toggleBtn.onclick = grid.toggleSidebar;
};

let grid = null;

const onInit = () => {
    setupGrid([])
};


onInit();
